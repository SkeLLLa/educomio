package io.educom.m03geek.educomio.ui;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Map;

import io.educom.m03geek.educomio.R;
import io.educom.m03geek.educomio.service.EducomIOService;
import io.educom.m03geek.educomio.service.model.LoginMessage;
import io.educom.m03geek.educomio.service.model.MessageTypes;
import io.educom.m03geek.educomio.service.model.NotificationMessage;


public class InOutActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    final String TAG = "EducomIO";

    ProgressBar progressBar;
    ToggleButton buttonStatus;
    EditText editStatus;
    Menu menu;
    MenuItem showGodMode;

    boolean isPersonInside = false;
    boolean isLoading = false;
    boolean mBounded;

    SharedPreferences preferences;

    boolean isNotificationsEnabled;
    boolean isNotificationsVibrateEnabled;
    boolean isGodModeEnabled = false;

    Intent serviceIntent;
    Messenger serviceMessenger;

    ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            serviceMessenger = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceMessenger = new Messenger(service);
            mBounded = true;
            Message message = Message.obtain(null, MessageTypes.ADD_ACTIVITY_HANDLER.getValue(), new ClientHandle());
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        unbindService(mConnection);
    }

    @Override
    public void onResume() {
        super.onResume();
        bindService(serviceIntent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_out);
        serviceIntent = new Intent(this, EducomIOService.class);
        startService(serviceIntent);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        buttonStatus = (ToggleButton) findViewById(R.id.toggleButton);
        editStatus = (EditText) findViewById(R.id.editStatus);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        isNotificationsEnabled = preferences.getBoolean("notifications_in_out", true);
        isNotificationsVibrateEnabled = preferences.getBoolean("notifications_in_out_vibrate", true);
        isGodModeEnabled = preferences.getBoolean("pref_try_external_login", false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.in_out, menu);
        this.menu = menu;
        showGodMode = menu.findItem(R.id.action_show_god_mode);
        showGodMode.setVisible(isGodModeEnabled);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        } else if(id == R.id.action_settings){

        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickToggleStatus(View v) {
        if (isLoading) {
            Toast.makeText(InOutActivity.this, "Шо ти клацаєш? Не бачиш - воно грузицця!", Toast.LENGTH_SHORT).show();
        } else {
            try {
                progressBar.setVisibility(View.VISIBLE);
                String messageText = editStatus.getText().toString();
                LoginMessage m = new LoginMessage(isPersonInside, messageText);
                serviceMessenger.send(Message.obtain(null, MessageTypes.MSG_GET_INOUT_RESULT.getValue(), m));
            } catch (RemoteException e) {
                Log.e(TAG, e.getMessage(), e.fillInStackTrace());
            }
        }
    }

    private void onRemoteAction(Boolean action){
        if (isLoading) {
            Toast.makeText(InOutActivity.this, "Шо ти клацаєш? Не бачиш - воно грузицця!", Toast.LENGTH_SHORT).show();
        } else {
            try {
                progressBar.setVisibility(View.VISIBLE);
                serviceMessenger.send(Message.obtain(null, MessageTypes.MSG_REMOTE_INOUT.getValue(), action));
            } catch (RemoteException e) {
                Log.e(TAG, e.getMessage(), e.fillInStackTrace());
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean result = super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_settings).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                InOutActivity.this.startActivity(new Intent(InOutActivity.this, SettingsActivity.class));
                return true;
            }
        });
        menu.findItem(R.id.action_status_view).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                InOutActivity.this.startActivity(new Intent(InOutActivity.this, EducomStatusActivity.class));
                return true;
            }
        });
        menu.findItem(R.id.action_status_view).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                InOutActivity.this.startActivity(new Intent(InOutActivity.this, EducomStatusActivity.class));
                return true;
            }
        });
        menu.findItem(R.id.action_god_mode_login).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                onRemoteAction(true);
                return true;
            }
        });
        menu.findItem(R.id.action_god_mode_logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                onRemoteAction(false);
                return true;
            }
        });
        return result;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        Log.d(TAG, "App preference has changed: " + s);
        if (s.equals("notifications_in_out")) {
            isNotificationsEnabled = sharedPreferences.getBoolean("notifications_in_out", true);
        } else if (s.equals("notifications_in_out_vibrate")){
            isNotificationsVibrateEnabled = sharedPreferences.getBoolean("notifications_in_out_vibrate", true);
        } else if (s.equals("pref_try_external_login")){
            isGodModeEnabled = sharedPreferences.getBoolean("pref_try_external_login", false);
            showGodMode.setVisible(isGodModeEnabled);
        }
    }

    public class ClientHandle extends Handler {
        @Override
        public void handleMessage(Message msg) {
            MessageTypes type = MessageTypes.NOTHING;
            try {
                type = MessageTypes.getType(msg.what);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d(TAG, "Activity:MessageType " + type + " : " + msg.what);
            switch (type) {
                case ADD_ACTIVITY_HANDLER:
                    Log.d(TAG, "Activity:" + msg.obj.toString());
                    try {
                        isLoading = true;
                        progressBar.setVisibility(View.VISIBLE);
                        serviceMessenger.send(Message.obtain(null, MessageTypes.MSG_GET_STATUS.getValue()));
                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage(), e.fillInStackTrace());
                    }
                    break;
                case MSG_GET_STATUS:
                    Log.d(TAG, "Activity:MSG_GET_STATUS " + msg.obj.toString());
                    if (msg.obj.equals("in")) {
                        isPersonInside = true;
                        Toast.makeText(InOutActivity.this, "Сейчас вы находитесь на работе", Toast.LENGTH_SHORT).show();
                    } else {
                        isPersonInside = false;
                        Toast.makeText(InOutActivity.this, "Сейчас вы НЕ находитесь на работе", Toast.LENGTH_SHORT).show();
                    }
                    buttonStatus.setChecked(isPersonInside);
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                    break;
                case MSG_ERROR:
                    String errorText = msg.obj.toString();
                    Log.d(TAG, "Activity:MSG_ERROR " + errorText);
                    Toast.makeText(InOutActivity.this, errorText, Toast.LENGTH_SHORT).show();
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                    break;
                case MSG_REMOTE_INOUT:
                    String text = "Мы сделали всё, что могли. Проверяйте!";
                    Toast.makeText(InOutActivity.this, text, Toast.LENGTH_SHORT).show();
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                    break;
                case MSG_SHOW_TOAST:
                    text = msg.obj.toString();
                    Toast.makeText(InOutActivity.this, text, Toast.LENGTH_SHORT).show();
                    break;
                case MSG_SEND_NOTIFICATION:
                    if (isNotificationsEnabled) {
                        NotificationMessage m = (NotificationMessage) msg.obj;
                        NotificationCompat.Builder builder =
                                new NotificationCompat.Builder(InOutActivity.this)
                                        .setSmallIcon(R.drawable.ic_launcher)
                                        .setContentTitle(m.getTitle())
                                        .setContentText(m.getText());
                        int NOTIFICATION_ID = 1;
                        Intent targetIntent = new Intent(InOutActivity.this, InOutActivity.class);
                        PendingIntent contentIntent = PendingIntent.getActivity(InOutActivity.this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentIntent(contentIntent);
                        builder.setAutoCancel(true);
                        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        nManager.notify(NOTIFICATION_ID, builder.build());

                        if (isNotificationsVibrateEnabled) {
                            Vibrator v = (Vibrator) InOutActivity.this.getSystemService(Context.VIBRATOR_SERVICE);

                            int dot = 200;      // Length of a Morse Code "dot" in milliseconds
                            int dash = 500;     // Length of a Morse Code "dash" in milliseconds
                            int short_gap = 200;    // Length of Gap Between dots/dashes
                            int medium_gap = 500;   // Length of Gap Between Letters
                            int long_gap = 1000;    // Length of Gap Between Words
                            long[] pattern;

                            if (m.getType().equalsIgnoreCase("login")) {
                                pattern = new long[]{
                                        0,  // Start immediately
                                        dot, short_gap, dot, short_gap, dot, short_gap, dot,    // H
                                        medium_gap,
                                        dot, short_gap, dot, // i
                                        long_gap
                                };
                                v.vibrate(pattern, -1);
                            } else if (m.getType().equalsIgnoreCase("logout")) {
                                pattern = new long[]{
                                        0,  // Start immediately
                                        dash, short_gap, dot, short_gap, dot, short_gap, dot,    // B
                                        medium_gap,
                                        dash, short_gap, dash, short_gap, dot, short_gap, dash, // y
                                        medium_gap,
                                        dot,    // e
                                        long_gap
                                };
                                v.vibrate(pattern, -1);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    }
}
