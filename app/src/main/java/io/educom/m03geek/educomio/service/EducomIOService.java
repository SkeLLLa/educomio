package io.educom.m03geek.educomio.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpResponse;

import java.util.Calendar;

import io.educom.m03geek.educomio.R;
import io.educom.m03geek.educomio.service.model.LoginMessage;
import io.educom.m03geek.educomio.service.model.MessageTypes;
import io.educom.m03geek.educomio.service.model.NotificationMessage;

public class EducomIOService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {

    final String TAG = "EducomIO";

    Looper mServiceLooper;
    ServiceHandler mServiceHandler;
    Messenger serviceMessenger;
    Messenger clientMessenger;

    SharedPreferences preferences;

    boolean status = false;
    boolean isWifiEnabled = false;
    boolean isInternetEnabled = false;
    String checkServerUrl;
    boolean isGodModeEnabled;

    BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "WIFI_DETECTOR: State change event received");
            String action = intent.getAction();
            if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (conMan != null) {
                    NetworkInfo netInfo = conMan.getActiveNetworkInfo();
                    if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                        WifiManager wifiMgr = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
                        WifiInfo wifiInfo;
                        String ssid = "";
                        if (wifiMgr != null) {
                            wifiInfo = wifiMgr.getConnectionInfo();
                            if (wifiInfo != null) {
                                ssid = wifiInfo.getSSID();
                            }
                            if (ssid.equals("")) {
                                String netExtraInfo = netInfo.getExtraInfo();
                                if (netExtraInfo != null) {
                                    ssid = netExtraInfo;
                                }
                            }

                            if ((ssid.equalsIgnoreCase("educom") || ssid.equalsIgnoreCase("\"educom\""))) {
                                isWifiEnabled = true;
                                isInternetEnabled = false;
                                Log.d(TAG, "WIFI_DETECTOR: Set wifi enabled");
                                /* store internal educom ip */
                                if (wifiInfo != null && preferences.getString("pref_educom_ip", "").equals("")) {
                                    int ip = wifiInfo.getIpAddress();
                                    String ipString = String.format(
                                            "%d.%d.%d.%d",
                                            (ip & 0xff),
                                            (ip >> 8 & 0xff),
                                            (ip >> 16 & 0xff),
                                            (ip >> 24 & 0xff));

                                    //preferences.getBoolean("notifications_in_out", true);
                                    preferences.edit().putString("pref_educom_ip", ipString).commit();
                                }
                                AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(checkServerUrl + "personal_status.php"), new AsyncHttpClient.StringCallback() {
                                    @Override
                                    public void onCompleted(Exception exGetStatus, AsyncHttpResponse response, String result) {
                                        if (exGetStatus != null) {
                                            status = false;
                                        } else {
                                            result = result.replaceAll("\\s+", "");
                                            status = result.equalsIgnoreCase("in");

                                            try {
                                                clientMessenger.send(Message.obtain(null, MessageTypes.MSG_GET_STATUS.getValue(), result));
                                            } catch (RemoteException e) {
                                                Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                                            }

                                            if (!status && clientMessenger != null) {
                                                Calendar calendar = Calendar.getInstance();
                                                int day = calendar.get(Calendar.DAY_OF_WEEK);
                                                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                                                if (day > Calendar.SUNDAY && day < Calendar.SATURDAY && hour >= 8 && hour <= 12) {
                                                    NotificationMessage m = new NotificationMessage("login", getString(R.string.msg_hello_educom_title), getString(R.string.msg_hello_educom_text));
                                                    try {
                                                        clientMessenger.send(Message.obtain(null, MessageTypes.MSG_SEND_NOTIFICATION.getValue(), m));
                                                    } catch (RemoteException e1) {
                                                        Log.e(TAG, "Activity communication failed: " + e1.getMessage(), e1.fillInStackTrace());
                                                    }
                                                }
                                            }
                                        }
                                        return;
                                    }
                                });
                                return;
                            }
                        }
                    }
                }
                isWifiEnabled = false;
                isInternetEnabled = true;
                Log.d(TAG, "WIFI_DETECTOR: Set wifi disabled");
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_WEEK);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                if (day > Calendar.SUNDAY && day < Calendar.SATURDAY && hour >= 17 && hour <= 21) {
                    if (status && clientMessenger != null) {
                        try {
                            NotificationMessage m = new NotificationMessage("logout", getString(R.string.msg_leave_educom_title), getString(R.string.msg_leave_educom_text));
                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_SEND_NOTIFICATION.getValue(), m));
                        } catch (RemoteException e) {
                            Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                        }
                    }
                }
            }
        }
    };

    public EducomIOService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Educom service created");
        HandlerThread thread = new HandlerThread("EducomIOServiceHandler", Thread.MIN_PRIORITY);
        thread.start();
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        serviceMessenger = new Messenger(mServiceHandler);
        if (serviceReceiver != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(serviceReceiver, intentFilter);
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        checkServerUrl = preferences.getString("pref_check_server_url", "http://192.168.1.55/");
        isGodModeEnabled = preferences.getBoolean("pref_try_external_login", false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(serviceReceiver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return serviceMessenger.getBinder();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals("pref_check_server_url")) {
            checkServerUrl = sharedPreferences.getString("pref_check_server_url", "http://192.168.1.55/");
        } else if (s.equals("pref_try_external_login")) {
            isGodModeEnabled = sharedPreferences.getBoolean("pref_try_external_login", false);
        }
    }

    public class ServiceHandler extends Handler {
        AsyncHttpClient httpClient;

        public ServiceHandler(Looper looper) {
            super(looper);
            httpClient = new AsyncHttpClient(AsyncServer.getCurrentThreadServer());
        }

        @Override
        public void handleMessage(Message msg) {
            MessageTypes type = MessageTypes.NOTHING;
            try {
                type = MessageTypes.getType(msg.what);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e.fillInStackTrace());
            }
            switch (type) {
                case ADD_ACTIVITY_HANDLER:
                    try {
                        clientMessenger = new Messenger((Handler) msg.obj);
                        clientMessenger.send(Message.obtain(null, MessageTypes.ADD_ACTIVITY_HANDLER.getValue(), "Client messenger registered"));
                    } catch (RemoteException e) {
                        Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                    }
                    break;
                case MSG_GET_STATUS:
                    if (isWifiEnabled) {
                        AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(checkServerUrl + "personal_status.php"), new AsyncHttpClient.StringCallback() {
                            @Override
                            public void onCompleted(Exception exGetStatus, AsyncHttpResponse response, String result) {
                                if (exGetStatus != null) {
                                    try {
                                        clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_communication_failed)));
                                        Log.e(TAG, "Communication failed: " + exGetStatus.getMessage(), exGetStatus.fillInStackTrace());
                                    } catch (RemoteException exGetStatus1) {
                                        Log.e(TAG, "Activity communication failed: " + exGetStatus1.getMessage(), exGetStatus1.fillInStackTrace());
                                    }
                                } else {
                                    try {
                                        result = result.replaceAll("\\s+", "");
                                        if (result.equalsIgnoreCase("in")) {
                                            status = true;
                                        } else {
                                            status = false;
                                        }
                                        clientMessenger.send(Message.obtain(null, MessageTypes.MSG_GET_STATUS.getValue(), result));
                                    } catch (RemoteException exGetStatus1) {
                                        Log.e(TAG, "Activity communication failed: " + exGetStatus1.getMessage(), exGetStatus1.fillInStackTrace());
                                    }
                                }
                            }
                        });
                    } else {
                        try {
                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_wifi_not_connected)));
                        } catch (RemoteException e) {
                            Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                        }
                    }
                    break;
                case MSG_REMOTE_INOUT:
                    if (isInternetEnabled && isGodModeEnabled) {
                        //try from internet
                        Boolean action = (Boolean) msg.obj;
                        String ip = preferences.getString("pref_educom_ip", "");
                        if (!ip.equals("")) {
                            Uri url = Uri.parse("http://educom.zxc.pp.ua/hitrozhop" + (action ? "?action=in" : "?action=out") + "&ip=" + ip);
                            AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(url), new AsyncHttpClient.StringCallback() {
                                @Override
                                public void onCompleted(Exception exGetInOut, AsyncHttpResponse response, String result) {
                                    if (exGetInOut != null) {
                                        try {
                                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_communication_failed)));
                                            Log.e(TAG, "Communication failed: " + exGetInOut.getMessage(), exGetInOut.fillInStackTrace());
                                        } catch (RemoteException exGetInOut1) {
                                            Log.e(TAG, "Activity communication failed: " + exGetInOut1.getMessage(), exGetInOut1.fillInStackTrace());
                                        }
                                    } else {
                                        try {
                                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_REMOTE_INOUT.getValue()));
                                        } catch (RemoteException exGetStatus1) {
                                            Log.e(TAG, "Activity communication failed: " + exGetStatus1.getMessage(), exGetStatus1.fillInStackTrace());
                                        }
                                    }
                                }
                            });
                        }
                    } else {
                        try {
                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_communication_failed)));
                        } catch (RemoteException e) {
                            Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                        }
                    }
                    break;
                case MSG_GET_INOUT_RESULT:
                    if (isWifiEnabled) {
                        LoginMessage m = (LoginMessage) msg.obj;
                        Uri url = Uri.parse(checkServerUrl +
                                        (m.getCurrentStatus() ? "?action=out" : "?action=in"
                                                + "&comment=" + m.getMessageText()).replace(' ', '+')
                        );
                        AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(url), new AsyncHttpClient.StringCallback() {
                            @Override
                            public void onCompleted(Exception exGetInOut, AsyncHttpResponse response, String result) {
                                if (exGetInOut != null) {
                                    try {
                                        clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_communication_failed)));
                                        Log.e(TAG, "Communication failed: " + exGetInOut.getMessage(), exGetInOut.fillInStackTrace());
                                    } catch (RemoteException exGetInOut1) {
                                        Log.e(TAG, "Activity communication failed: " + exGetInOut1.getMessage(), exGetInOut1.fillInStackTrace());
                                    }
                                } else {
                                    AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(checkServerUrl + "personal_status.php"), new AsyncHttpClient.StringCallback() {
                                        @Override
                                        public void onCompleted(Exception exGetStatus, AsyncHttpResponse response, String result) {
                                            if (exGetStatus != null) {
                                                try {
                                                    clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_communication_failed)));
                                                    Log.e(TAG, "Communication failed: " + exGetStatus.getMessage(), exGetStatus.fillInStackTrace());
                                                } catch (RemoteException exGetStatus1) {
                                                    Log.e(TAG, "Activity communication failed: " + exGetStatus1.getMessage(), exGetStatus1.fillInStackTrace());
                                                }
                                            } else {
                                                try {
                                                    result = result.replaceAll("\\s+", "");
                                                    if (result.equalsIgnoreCase("in")) {
                                                        status = true;
                                                    } else {
                                                        status = false;
                                                    }
                                                    clientMessenger.send(Message.obtain(null, MessageTypes.MSG_GET_STATUS.getValue(), result));
                                                } catch (RemoteException exGetStatus1) {
                                                    Log.e(TAG, "Activity communication failed: " + exGetStatus1.getMessage(), exGetStatus1.fillInStackTrace());
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        try {
                            clientMessenger.send(Message.obtain(null, MessageTypes.MSG_ERROR.getValue(), getString(R.string.error_wifi_not_connected)));
                        } catch (RemoteException e) {
                            Log.e(TAG, "Activity communication failed: " + e.getMessage(), e.fillInStackTrace());
                        }
                    }
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    }
}
