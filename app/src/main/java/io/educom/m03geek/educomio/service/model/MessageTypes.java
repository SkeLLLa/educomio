package io.educom.m03geek.educomio.service.model;

/**
 * Created by m03geek on 08.07.14.
 */
public enum MessageTypes {
    NOTHING(0),
    ADD_ACTIVITY_HANDLER(1),
    MSG_GET_STATUS(2),
    MSG_GET_INOUT_RESULT(3),
    MSG_SEND_NOTIFICATION(4),
    MSG_SHOW_TOAST(5),
    MSG_ERROR(6),
    MSG_REMOTE_INOUT(7);

    private int typeValue;

    private MessageTypes(int type) {
        typeValue = type;
    }

    static public MessageTypes getType(int pType) {
        for (MessageTypes type: MessageTypes.values()) {
            if (type.getValue() == pType) {
                return type;
            }
        }
        throw new RuntimeException("Unknown message type");
    }

    public int getValue() {
        return typeValue;
    }
}
