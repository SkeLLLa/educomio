package io.educom.m03geek.educomio.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import io.educom.m03geek.educomio.R;

public class EducomStatusActivity extends ActionBarActivity {

    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_educom_status);
        wv = (WebView) findViewById(R.id.webView);
    }

    @Override
    public void onResume() {
        super.onResume();

        ConnectivityManager conMan = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();

        if (netInfo != null) {
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI && netInfo.getExtraInfo().equalsIgnoreCase("\"educom\"")) {
                wv.setNetworkAvailable(true);
                wv.loadUrl("http://192.168.1.55/status.php");
            } else {
                wv.setNetworkAvailable(true);
                wv.loadUrl("http://educom.zxc.pp.ua/checkin/status.php");
            }
        } else {
            //show toast
            Toast.makeText(EducomStatusActivity.this, "Нет подключения к интернету", Toast.LENGTH_SHORT).show();
            wv.setNetworkAvailable(false);
        }
    }
}
