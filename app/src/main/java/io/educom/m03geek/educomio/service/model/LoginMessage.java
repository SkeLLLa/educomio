package io.educom.m03geek.educomio.service.model;

/**
 * Created by m03geek on 08.07.14.
 */
public class LoginMessage {
    private Boolean currentStatus;

    public Boolean getCurrentStatus() {
        return currentStatus;
    }

    public String getMessageText() {
        return messageText;
    }

    private String messageText;

    public LoginMessage(Boolean currentStatus, String messageText) {
        this.currentStatus = currentStatus;
        this.messageText = messageText;
    }
}
