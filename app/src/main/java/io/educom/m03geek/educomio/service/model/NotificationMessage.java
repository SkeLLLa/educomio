package io.educom.m03geek.educomio.service.model;

/**
 * Created by m03geek on 10.07.14.
 */
public class NotificationMessage {
    private String title;
    private String text;
    private String type;

    public NotificationMessage(String type, String title, String text) {
        this.title = title;
        this.text = text;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }
}
